import './App.css';
import { Route, Switch } from 'react-router-dom';

import Members from './members';

import Home from './components/Home';
import Customer from './pages/customer';
import Company from './pages/company.jsx';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route exact path="/">
            <Home members={Members}/>
          </Route>
          <Route path="/customer/:id">
            <Customer members={Members}/>
          </Route>
          <Route path="/company/:id">
            <Company members={Members}/>
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
