import { Link } from 'react-router-dom';

const Home = ({ members }) => {
    return (
        <>
            {members.map((item, index) => 
                item.type === 'pf' ? 
                <Link key={index} to={`/customer/${item.id}`}>{item.name}</Link>
                :
                <Link key={index} to={`/company/${item.id}`}>{item.name}</Link>
            )}
        </>
    );
}

export default Home;