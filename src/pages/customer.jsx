import { useParams, Link } from 'react-router-dom';

const Customer = ({ members }) => {
    const params = useParams();
    const member = members.find(member => member.id === params.id);

    return (
        <div>
            <h1>Detalhes do cliente</h1>

            <Link to='/'>
                Voltar
            </Link>

            <p>Nome: {member && member.name}</p>
        </div>
    );
}

export default Customer;