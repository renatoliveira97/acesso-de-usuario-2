import { useParams, Link } from 'react-router-dom';

const Company = ({ members }) => {
    const params = useParams();
    const member = members.find(member => member.id === params.id);

    return (
        <div>
            <h1>Detalhes da Empresa</h1>

            <Link to='/'>
                Voltar
            </Link>

            <p>Nome da empresa: {member && member.name}</p>
        </div>
    );
}

export default Company;